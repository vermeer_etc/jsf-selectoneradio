# JSF で SelectOneRadio

## Description

JSF のラジオボタンで色々と お試し実装をしてみる。

## Usage

## Licence

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Author

[_vermeer_](https://twitter.com/_vermeer_)
