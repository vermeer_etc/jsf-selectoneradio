/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.table;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Yamashita,Takahiro
 */
@Named
@RequestScoped
public class ItemAction {

    ItemsForm form;

    public ItemAction() {
    }

    @Inject
    public ItemAction(ItemsForm form) {
        this.form = form;
    }

    public String confirm() {
        return "confirm.xhtml";
    }

    public void update() {
    }

    public void change(Integer index) {
        this.form.updateCheckedItem(index);
    }

}
