/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Yamashita,Takahiro
 */
@Named
@SessionScoped
public class ItemsForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<ItemForm> items;

    private String checkedItem;

    @PostConstruct
    public void init() {
        List<ItemForm> _items = new ArrayList<>();
        _items.add(new ItemForm("a1", "ＡＡ　ＡＡ"));
        _items.add(new ItemForm("b1", "ＢＢ　ＢＢ"));
        _items.add(new ItemForm("c1", "ＣＣ　ＣＣ"));
        this.items = _items;

        checkedItem = _items.get(0).getItemId();
    }

    public boolean renderChecked(Integer itemIndex) {
        return Objects.equals(this.checkedItem, this.items.get(itemIndex).getItemId());
    }

    public Map<String, String> getChecked() {
        Map<String, String> map = new HashMap<>();
        map.put("checked", "checked");
        return map;
    }

    public List<ItemForm> getItems() {
        return items;
    }

    public void setItems(List<ItemForm> items) {
        this.items = items;
    }

    public String getCheckedItem() {
        return checkedItem;
    }

    public void setCheckedItem(String checkedItem) {
        this.checkedItem = checkedItem;
    }

    public void updateCheckedItem(Integer itemIndex) {
        this.checkedItem = this.items.get(itemIndex).getItemId();
    }

}
