/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.datatable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Yamashita,Takahiro
 */
public class UserForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private Boolean checked;

    private final String id;

    private final String name;

    public UserForm(String index, String name) {
        this.checked = false;
        this.id = index;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getChecked() {
        System.out.println("checked2:::");

        Map<String, String> map = new HashMap<>();
        map.put("name2", "userJSF2");
        return map;

    }

//    public void setChecked(Boolean checked) {
//        this.checked = checked;
//    }
}
