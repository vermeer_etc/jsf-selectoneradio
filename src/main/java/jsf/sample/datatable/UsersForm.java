/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.datatable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;

/**
 *
 * @author Yamashita,Takahiro
 */
@Named
@SessionScoped
public class UsersForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private DataModel<UserForm> dataModel;

    private List<UserForm> users;

    private String checkedId;

    @PostConstruct
    public void init() {
        List<UserForm> _users = new ArrayList<>();
        _users.add(new UserForm("0", "ああ　ああ"));
        _users.add(new UserForm("1", "いい　いい"));
        _users.add(new UserForm("2", "うう　うう"));

        ListDataModel<UserForm> _datamodel = new ListDataModel<>();

        this.users = _users;
        _datamodel.setWrappedData(_users);
        this.dataModel = _datamodel;

        this.checkedId = _users.get(0).getId();
    }

    public Map<String, String> checked(Integer rowIndex) {
        System.out.println("checked:::" + rowIndex);
        Map<String, String> map = new HashMap<>();
        map.put("name", "userJSF");
        if (rowIndex < 0) {
            return map;
        }
        if (this.hasChecked(rowIndex)) {
            map.put("checked", "checked");
        }

        return map;
    }

    Boolean hasChecked(Integer rowIndex) {
        List<UserForm> _users = (List<UserForm>) this.dataModel.getWrappedData();
        return Objects.equals(this.checkedId, _users.get(rowIndex).getId());
    }

    public String getCheckedId() {
        return checkedId;
    }

    public void setCheckedId(String checkedId) {
        this.checkedId = checkedId;
    }

    public DataModel<UserForm> getDataModel() {
        return dataModel;
    }

    public void setDataModel(DataModel<UserForm> dataModel) {
        this.dataModel = dataModel;
    }

    public List<UserForm> getUsers() {
        return users;
    }

    public void setUsers(List<UserForm> users) {
        this.users = users;
    }

}
