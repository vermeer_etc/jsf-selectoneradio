package jsf.sample.normal;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class GenderAction implements Serializable {

    private static final long serialVersionUID = 1L;

    private GenderForm genderForm;

    public GenderAction() {
    }

    @Inject
    public GenderAction(GenderForm genderForm) {
        this.genderForm = genderForm;
    }

    public void update() {
    }

    public String confirm() {
        return "confirm.xhtml";
    }

    public void change(AjaxBehaviorEvent event) {
        this.genderForm.setGenderType(event.getComponent().getId());
    }
}
