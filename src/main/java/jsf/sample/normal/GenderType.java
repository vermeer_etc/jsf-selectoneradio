/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.normal;

/**
 *
 * @author Yamashita,Takahiro
 */
public enum GenderType {

    MAN(0, "男性"),
    WOMAN(1, "女性"),
    OTHER(2, "不明");

    private final Integer value;
    private final String display;

    private GenderType(Integer value, String display) {
        this.value = value;
        this.display = display;
    }

    public Integer getValue() {
        return value;
    }

    public String getDisplay() {
        return display;
    }

    public static GenderType createGenderType(Integer value) {
        for (GenderType _genderType : GenderType.values()) {
            if (_genderType.getValue().equals(value)) {
                return _genderType;
            }
        }
        return GenderType.MAN;
    }

}
