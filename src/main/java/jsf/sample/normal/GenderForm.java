/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.normal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

/**
 *
 * @author Yamashita,Takahiro
 */
@Named
@SessionScoped
public class GenderForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private GenderType genderType;

    @PostConstruct
    public void init() {
        this.genderType = GenderType.MAN;
    }

    public List<SelectItem> getSelectItems() {
        List<SelectItem> items = new ArrayList<>();

        for (GenderType _genderType : GenderType.values()) {
            items.add(new SelectItem(String.valueOf(_genderType.getValue()), _genderType.getDisplay()));
        }

        return items;
    }

    public Integer getGenderValue() {
        return this.genderType.getValue();
    }

    public void setGenderValue(Integer genderValue) {
        this.genderType = GenderType.createGenderType(genderValue);
    }

    public GenderType getGenderType() {
        return this.genderType;
    }

    public void setGenderType(String genderName) {
        this.genderType = GenderType.valueOf(genderName);
    }

    public Map<String, String> checked(Integer genderIndex) {
        Map<String, String> map = new HashMap<>();
        if (this.hasChecked(genderIndex)) {
            map.put("checked", "checked");
        }
        return map;
    }

    //
    Boolean hasChecked(Integer genderIndex) {
        return Objects.equals(this.genderType.getValue(), genderIndex);
    }

    public String targetId(Integer genderIndex) {
        GenderType[] genderTypes = GenderType.values();
        return genderTypes[genderIndex].name();
    }

    public String targetLabel(Integer genderIndex) {
        GenderType[] genderTypes = GenderType.values();
        return genderTypes[genderIndex].getDisplay();
    }

    public String targetFor(String component, Integer genderIndex) {
        return component + "-" + this.targetId(genderIndex);
    }

}
