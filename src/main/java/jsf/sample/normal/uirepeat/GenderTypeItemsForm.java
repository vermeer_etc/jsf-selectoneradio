/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.normal.uirepeat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.inject.Named;
import jsf.sample.normal.GenderType;

/**
 *
 * @author Yamashita,Takahiro
 */
@Named
@SessionScoped
public class GenderTypeItemsForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<GenderTypeForm> items;

    private GenderType genderType;

    @PostConstruct
    public void init() {
        List<GenderTypeForm> _items = new ArrayList<>();
        _items.add(new GenderTypeForm(GenderType.MAN));
        _items.add(new GenderTypeForm(GenderType.WOMAN));
        _items.add(new GenderTypeForm(GenderType.OTHER));
        this.items = _items;
        this.genderType = GenderType.MAN;
    }

    public List<GenderTypeForm> getItems() {
        return items;
    }

    public void setItems(List<GenderTypeForm> items) {
        this.items = items;
    }

    public Map<String, String> checked() {
        Map<String, String> map = new HashMap<>();
        map.put("checked", "checked");
        return map;
    }

    public boolean renderChecked(Integer itemIndex) {
        return Objects.equals(this.genderType.getValue(), this.items.get(itemIndex).getGenderTypeValue());
    }

    public String getDisplay() {
        return this.genderType.getDisplay();
    }

    public void updateGenderType(Integer index) {
        this.genderType = this.items.get(index).getGenderType();
    }

    public String forTargetRadioOn(UIComponent component) {
        return component.getParent().getClientId() + "-radioOn";
    }

    public String forTargetRadioOff(UIComponent component) {
        return component.getParent().getClientId() + "-radioOff";
    }

}
