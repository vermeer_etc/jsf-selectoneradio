/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package jsf.sample.normal.uirepeat;

import java.io.Serializable;
import jsf.sample.normal.GenderType;

public class GenderTypeForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private GenderType genderType;

    public GenderTypeForm(GenderType genderType) {
        this.genderType = genderType;
    }

    public Integer getGenderTypeValue() {
        return genderType.getValue();
    }

    public void setGenderTypeValue(Integer genderValue) {
        this.genderType = GenderType.createGenderType(genderValue);
    }

    public String getDisplay() {
        return this.genderType.getDisplay();
    }

    public GenderType getGenderType() {
        return this.genderType;
    }
}
