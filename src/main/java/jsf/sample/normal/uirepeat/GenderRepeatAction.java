package jsf.sample.normal.uirepeat;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class GenderRepeatAction implements Serializable {

    private static final long serialVersionUID = 1L;

    private GenderTypeItemsForm genderTypesForm;

    public GenderRepeatAction() {
    }

    @Inject
    public GenderRepeatAction(GenderTypeItemsForm genderTypesForm) {
        this.genderTypesForm = genderTypesForm;
    }

    public void update() {
    }

    public String confirm() {
        return "confirm.xhtml";
    }

    public void change(Integer index) {
        this.genderTypesForm.updateGenderType(index);
    }
}
